//
// Created by sortofsleepy on 6/8/2023.
//

#ifndef SWIFT_TEST_MATH_H
#define SWIFT_TEST_MATH_H

class Math {
public:
    Math();
    int add(int a, int b);

};

#endif //SWIFT_TEST_MATH_H
