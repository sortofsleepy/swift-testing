Swift Test
===

A simple test of Swift on Windows. Includes testing Cpp integration. 

Setup Notes
=== 
* Follow the directions on the [Swift.org](https://swift.org/download) website. For reference, I downloaded the official release.
* You technically don't need to enable developer mode on Windows, at least for simple examples from what I can gather. If you leave
it off, you will get a warning though which you should be able to ignore for the time being. 
* For Cpp integration, on Windows, it seems that the current instructions are slightly incorrect(or I did something wrong.)
  * I had to move my cpp files outside of the main Swift source directory, otherwise you get an error saying there is mixed content. 
  * the `.interoperabilityMode` flag doesn't appear to exist(at least on Windows), instead use `.unsafeFlags(["-enable-experimental-cxx-interop"])`
  * If you're using CLion, the Swift plugin from Jetbrains is broken(and possibly deprecated), debugging might not be possible but you should be able to still 
  compile and run by running `swift run` in the terminal.