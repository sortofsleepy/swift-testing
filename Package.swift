// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
        name: "swift_test",
        products: [
            .library(
                    name: "cpp",
                    targets: ["cpp"]
            ),
            .executable(
                    name: "swift_test",
                    targets: ["swift_test"]
            )
        ],
        targets: [
            .target(name: "cpp", path: "./cpp"),
            // Targets are the basic building blocks of a package, defining a module or a test suite.
            // Targets can depend on other targets in this package and products from dependencies.
            .executableTarget(
                    name: "swift_test",
                    dependencies: ["cpp"],
                    path: "Sources",
                    swiftSettings: [.unsafeFlags(["-enable-experimental-cxx-interop"])]),

            // this doesn't quite work yet, at least on windows.
            //  swiftSettings: [.interoperabilityMode(.Cxx)]),
        ]
)
